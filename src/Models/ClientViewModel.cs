﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ClientDB;

namespace Estoque_Rodrigo.Models
{
    public class ClientViewModel : AddressViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string CEP { get; set; }
        public TipoEmpresa TipoEmpresa { get; set; }
        public bool Inativo { get; set; }

        public bool Compras { get; set; }
        public bool Vendas { get; set; }
        public bool Deposito { get; set; }
        //public List<SubSystem> SubSystems { get; set; }
    }

    //public class SubSystem
    //{
    //    public bool UseFlag { get; set; }
    //    public SubSystem_Types System { get; set; }
    //    public Role_Types Role { get; set; }

       
    //}


}