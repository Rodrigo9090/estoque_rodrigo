﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClientDB;
namespace Estoque_Rodrigo.Models
{
    public class ContactViewModel : AddressViewModel
    {
        public int Id { get; set; }
        public string Empresa { get; set; }
        public string Titulo { get; set; }
        public string Nome { get; set; }
        public Sexo_Tipo Sexo { get; set; }
        public TipoContato TipoContato { get; set; }
         public string Telefone { get; set; }
         public string Celular { get; set; }
         public string Fax { get; set; }
        public string Email { get; set; }
        public string Comentarios { get; set; }

    }
}