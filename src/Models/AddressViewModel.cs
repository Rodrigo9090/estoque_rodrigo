﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ClientDB;
namespace Estoque_Rodrigo.Models
{
    public class AddressViewModel
    {
        public Address_Types AddressType { get; set; }
        public int Id { get; set; }
        [Required]
        public string Endereco1 { get; set; }
        public string Endereco2 { get; set; }
        [Required]
        public string Cidade { get; set; }
        [Required]
        public string Estado { get; set; }
   
        public string Pais { get; set; }
        [Required]
        public string CEP { get; set; }
    
    }
}