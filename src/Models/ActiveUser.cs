﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Estoque_Rodrigo.Models
{
    public class ActiveUser
    {
        public int ClientId { get; set;}
        public string ID { get; set; }
        public String Nome { get; set; }
        public int FuncaoCliente { get; set; }

        public int FuncaoCompra { get; set; }
        public int Item { get; set; }
        public int FuncaoVendas { get; set; }
        public int Funcao { get; set; }
       
    }
}