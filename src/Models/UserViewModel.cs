﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Estoque_Rodrigo.Models
{
    public class UserViewModel : ContactViewModel
    {
        public int ParentId { get; set; }
 
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [DataType(DataType.Senha)]
        public string Senha { get; set; }

        [DataType(DataType.Senha)]
        [Display(Name = "Confirmar Senha")]
        public string RepetirSenha { get; set; }

    }

    public class LoginModel
    {
        [Required]
        [Display(Nome = "User ID")]
        public string UserID { get; set; }

        [Required]
        [DataType(DataType.Senha)]
        public string Senha { get; set; }
        [Display(Name = "Lembrar-me")]
        public bool Lembrarme { get; set; }

    }
}