﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Estoque_Rodrigo.Models
{
    public class OptionsViewModel
    {
        public string Subsistema { get; set; }
        public string Base { get; set; }
        public String FreqUEnciafaturamento { get; set; }
        public int FaixaBaixa { get; set; }
        public int FaixaAlt { get; set; }
        public decimal Pareco { get; set; }

    }

    public class OptionsListModel
    {
        public OptionsViewModel[] OptionsList { get; set; }
        public decimal PrecoTotal { get; set; }
    }
}