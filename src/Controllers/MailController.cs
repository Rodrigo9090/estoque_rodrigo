﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Estoque_Rodrigo.Models;
using ClientDB;
using CATRAN_Mail;

namespace Estoque_Rodrigo.Controllers
{
    public class MailController : Controller
    {
        private Estoque_RodrigoClientEntities1 db = new ABC_ClientEntities1();
        // GET: Contact
        public ActionResult MailView()
        {
            MailViewModel cvm = new MailViewModel();
            return View(cvm);
        }
        [HttpPost]
        public ActionResult MailView(MailViewModel cvm)
        {
            ContactHistory ch = new ContactHistory();
            Common.CopyProperties(cvm, ch, false);
            ch.ContactDate = DateTime.Now;
            db.ContactHistories.Add(ch);
            string Message = "";
            Message += "Data:" + ch.ContatoData + "\n\r";
            Message += "Empresa,: " + ch.Empresa + "\n\r";
            Message += "Nome: " + ch.ContatoNome + "\n\r";
            Message += "Telefone: " + ch.Telefone + "\n\r";
            Message += "Email: " + ch.Email + "\n\r";
            Message += "Mensagem: " + ch.Comentarios + "\n\r";
            Class_Gmail mail = new Class_Gmail(ch.Email, ch.ContactName, "hjh@gmail.com", "Customer Service", "hjh@gmail.com", "hjh");
            mail.Subject = "Inquiry";
            mail.Body = Message;      
            mail.Send();      
            db.SaveChanges();
            return View(cvm);
        }
    }
}